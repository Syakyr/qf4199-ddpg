from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="QF4199-tfagents",
    version="0.0.1",
    author="Syakyr Surani",
    author_email="coding@esyakyr.com",
    description="Reinforcement Learning Algorithms on Financial Markets",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    license='BSD',
    keywords=['reinforcement learning', 'ddpg', 'forex'],
    packages=find_packages(exclude=['tests']),
    install_requires=['tf-agents'],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha'
        'Programming Language :: Python :: 3',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
    ],
)
