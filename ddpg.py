from os import environ
environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from tensorflow.compat.v1.train import AdamOptimizer, AdagradOptimizer
from tf_agents.environments.tf_py_environment import TFPyEnvironment
from tf_agents.environments.parallel_py_environment import ParallelPyEnvironment
from tf_agents.agents import DdpgAgent
from tf_agents.agents.ddpg.actor_network import ActorNetwork
from tf_agents.agents.ddpg.critic_network import CriticNetwork
from tf_agents.replay_buffers.tf_uniform_replay_buffer import TFUniformReplayBuffer
from tf_agents.drivers.dynamic_step_driver import DynamicStepDriver
from tf_agents.drivers.dynamic_episode_driver import DynamicEpisodeDriver
from tf_agents.policies.greedy_policy import GreedyPolicy
from tf_agents.utils.common import function
from tqdm import tqdm, trange
from pathos.multiprocessing import Pool
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from argparse import ArgumentParser, ArgumentTypeError

from qf4199_tfagents.environment import SimMktEnv
from qf4199_tfagents.utils import *

VERSION = '0.1.2'

def evaluate_wealth_and_action(reset_action):
    time_step = reset_action[0].reset()
    wealth = time_step.observation.numpy()[0][0]
    action_step = reset_action[1](time_step)

    while not time_step.is_last():
        time_step = reset_action[0].step(action_step.action)
        action_step = reset_action[1](time_step)
        wealth = np.concatenate(
            (wealth, time_step.observation.numpy()[0][0]), axis=None
        )
    return wealth

def run(
    environment, env_args, eval_environment, eval_env_args,
    actor_fc_layer_params, critic_fc_layer_params, optimizer,
    optimizer_learning_rate, soft_update, replay_buffer_capacity, 
    sample_batch_size_pct,
    num_eval_episodes, num_iterations, filename, driver
):

    env = lambda: environment(*env_args)
    tf_env = TFPyEnvironment(env)

    env_eval = eval_environment(*eval_env_args)
    tf_env_eval = TFPyEnvironment(env_eval)
    
    ###
    
    actor = ActorNetwork(
        tf_env.observation_spec(),
        tf_env.action_spec(),
        fc_layer_params=actor_fc_layer_params
    )
    critic = CriticNetwork(
        (
            tf_env.observation_spec(),
            tf_env.action_spec()
        ),
        joint_fc_layer_params=critic_fc_layer_params
    )

    optim = lambda: optimizer(
        learning_rate=optimizer_learning_rate
    )
    
    ###
    
    ddpg = DdpgAgent(
        tf_env.time_step_spec(),
        tf_env.action_spec(),
        actor, critic,
        actor_optimizer=optim(),
        critic_optimizer=optim(),
        target_update_tau=soft_update
    )

    ddpg.initialize()
    
    ###
    
    replay_buffer = TFUniformReplayBuffer(
        data_spec=ddpg.collect_data_spec,
        batch_size=tf_env.batch_size,
        max_length=replay_buffer_capacity
    )
    sample_batch_size = round(replay_buffer_capacity*sample_batch_size_pct)
    
    eval_policy = GreedyPolicy(ddpg.policy)
    collect_policy = ddpg.policy
    
    collect_driver = driver(
        tf_env,
        collect_policy,
        observers=[replay_buffer.add_batch]
    )
    
    ###
    
    # Optimize by wrapping some of the code in a graph using TF function.
    ddpg.train = function(ddpg.train)
    collect_driver.run = function(collect_driver.run)
    
    # Reset the train step
    ddpg.train_step_counter.assign(0)

    if isinstance(collect_driver, DynamicStepDriver):
        [collect_driver.run() for _ in range(replay_buffer_capacity)]
        unit = 'step(s)'
    else:
        collect_driver.run() # DynamicEpisodeDriver
        unit = 'episode(s)'
    
    ###
        
    t = trange(num_iterations, unit=unit, desc='Training', leave=False)
    info = {}
    for _ in t:

        # Collect a few steps using collect_policy and save to the replay buffer.
        collect_driver.run()

        # Sample a batch of data from the buffer and update the agent's network.
        experience, _ = replay_buffer.get_next(
            sample_batch_size=sample_batch_size,
            num_steps=2
        )
        train_loss = ddpg.train(experience)

        t.set_postfix(**info)
        
    ###
    
    wealth_array = []

    np.random.seed(1729)

    for wealth in [evaluate_wealth_and_action(
        (tf_env_eval, eval_policy.action)
    ) for _ in trange(
        num_eval_episodes, desc='Evaluating', unit='episode(s)',leave=False
    )]:
        wealth_array.append(wealth)
    
    ###
    
    end_wealth = np.array([wealth[-1] for wealth in wealth_array])
    
    try:
        df = pd.read_csv(filename)
    except:
        df = pd.DataFrame(columns=['min', 'max', 'range', 'mean', 'median', 'std'])

    df = df.append({
        'min': end_wealth.min(), 'max': end_wealth.max(),
        'range': end_wealth.max()- end_wealth.min(),
        'mean': end_wealth.mean(), 'median': np.median(end_wealth),
        'std': end_wealth.std()
    }, ignore_index=True)

    df.to_csv(filename, index=None)
    
def main():
    parser = ArgumentParser(
        prog='emv-ddpg', usage='%(prog)s [options]',
        description='Running a DDPG algorithm on markets'
    )
    parser.add_argument(
        '-v', '--version', action='version', version=f'Version:%(prog)s-{VERSION}'
    )
    parser.add_argument(
        '--print', action='store_true',
        help='Prints the parameters.'
    )
    parser.add_argument(
        '-N', '--num-cycles', type=int, default=50,
        help='Number of run cycles. default=%(default)s'
    )
    parser.add_argument(
        '-o', '--output-file',
        help='Output file name'
    )
    parser.add_argument(
        '-e', '--environment', type=valid_environment, default=SimMktEnv,
        help='Environment to be used. default=SimMktEnv'
    )
    parser.add_argument(
        '-b', '--initial-balance', type=float, default=1.,
        help='default=%(default)s'
    )
    parser.add_argument(
        '--mu', type=float, default=0.5,
        help='default=%(default)s'
    )
    parser.add_argument(
        '--sigma', type=float, default=0.4,
        help='default=%(default)s'
    )
    parser.add_argument(
        '-r', '--risk-free', type=float, default=0.02,
        help='Risk free rate. default=%(default)s'
    )
    parser.add_argument(
        '-T', '--period', type=float, default=1.,
        help='The time period for mu, sigma and the risk free rate to '
             'follow. default=%(default)s'
    )
    parser.add_argument(
        '-D', '--delta', type=float, default=1/252,
        help='One time step as a fraction of the time period. '
             'default=%(default)s'
    )
    parser.add_argument(
        '-S', '--initial-price', type=float, default=1.,
        help='default=%(default)s'
    )
    parser.add_argument(
        '-t', '--target', type=float, default=1.4,
        help='Target multiple of the initial wealth (balance). '
             'default=%(default)s'
    )
    parser.add_argument(
        '-p', '--use-price', action='store_true',
        help='Use price as part of the observation value on top of '
             'wealth (balance). default=%(default)s'
    )
    parser.add_argument(
        '--price-file', help='Filename used to evaluate agent. If '
                             'None, it uses the parameters used in '
                             'training. default=None'
    )
    parser.add_argument(
        '--actor-fc-layer-params', type=int, nargs='*', default=(10,8),
        help='Number of nodes for each Dense layer in the actor '
             'network. default=10 8'
    )
    parser.add_argument(
        '--critic-fc-layer-params', type=int, nargs='*', default=(10,8,8),
        help='Number of nodes for each Dense layer in the critic '
             'network. default=10 8 8'
    )
    parser.add_argument(
        '--optimizer', type=valid_optimizer, default=AdamOptimizer,
        help='default=adam'
    )
    parser.add_argument(
        '--optimizer-learning-rate', type=float, default=0.0001,
        help='default=%(default)s'
    )
    parser.add_argument(
        '--soft-update', type=float, default=0.001,
        help='Soft update rate for the critic network. default=%(default)s'
    )
    parser.add_argument(
        '--replay-buffer-capacity', type=int, default=80,
        help='default=%(default)s'
    )
    parser.add_argument(
        '--sample-batch-size-pct', type=float, default=0.25,
        help='Sample batch size as a percentage of the replay buffer '
             'capacity. default=%(default)s'
    )
    parser.add_argument(
        '-d', '--driver', type=valid_driver, default=DynamicEpisodeDriver,
        help='Lets the agent know whether to train after each step or '
             'each episode. default=step'
    )
    parser.add_argument(
        '-E', '--num-eval-episodes', type=int, default=50,
        help='Number of episodes to run for evaluation. default=%(default)s'
    )
    parser.add_argument(
        '-i', '--num-iterations', type=int, default=400,
        help='Number of training iterations. default=%(default)s'
    )
    args = parser.parse_args()
    if args.print:
        print(args)
        return
    
    if args.output_file is None:
        output_file = 'stats.csv'
    else:
        output_file = f'{args.output_file}.csv'
    
    [run(
        args.environment, (
            args.initial_balance, args.mu, args.sigma, args.risk_free, 
            args.period, args.delta, args.initial_price, args.target, 
            args.use_price
        ),
        args.environment, (
            args.initial_balance, args.mu, args.sigma, args.risk_free, 
            args.period, args.delta, args.initial_price, args.target, 
            args.use_price, args.price_file, args.num_eval_episodes
        ),
        args.actor_fc_layer_params, args.critic_fc_layer_params, 
        args.optimizer, args.optimizer_learning_rate, args.soft_update, 
        args.replay_buffer_capacity, args.sample_batch_size_pct,
        args.num_eval_episodes, args.num_iterations, output_file, 
        args.driver
    ) for i in trange(args.num_cycles)]

if __name__ == '__main__':
    main()