import numpy as np
import pandas as pd

df = np.append(np.ones((200, 1)), np.random.normal(1.5**(1/252)-1, 1.4**np.sqrt(1/252)-1, size=(200,252))+1, axis=1).cumprod(axis=1)
df = pd.DataFrame(df).T
df.to_csv('sim_price.csv', index=None)