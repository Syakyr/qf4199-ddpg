#!/bin/bash

# Generate Simulated Prices
python generate_sim_price.py

# Round 1

# control
python ddpg.py --price-file sim_price.csv -o stats_control -N 50

# adagrad
python ddpg.py --price-file sim_price.csv -o stats_adagrad -N 50 --optimizer adagrad

# 2x sample batch size %
python ddpg.py --price-file sim_price.csv -o stats_batch -N 50 --sample-batch-size-pct 0.5

# 2x layers
python ddpg.py --price-file sim_price.csv -o stats_layer -N 50 --actor-fc-layer-params 10 10 8 8 --critic-fc-layer-params 10 10 8 8 8 8

# 2x nodes
python ddpg.py --price-file sim_price.csv -o stats_node -N 50 --actor-fc-layer-params 20 16 --critic-fc-layer-params 20 16 16

# price
python ddpg.py --price-file sim_price.csv -o stats_price -N 50 -p

# 10x replay capacity
python ddpg.py --price-file sim_price.csv -o stats_rb -N 50 --replay-buffer-capacity 1600

# step
python ddpg.py --price-file sim_price.csv -o stats_step -N 50 -d step -i 20000

# Round 2

# 4x nodes
python ddpg.py --price-file sim_price.csv -o stats_node4 -N 50 --actor-fc-layer-params 40 32 --critic-fc-layer-params 40 32 32

# 2x nodes, price
python ddpg.py --price-file sim_price.csv -o stats_node_price -N 50 --actor-fc-layer-params 20 16 --critic-fc-layer-params 20 16 16 -p

# 2x nodes, 10x replay capacity
python ddpg.py --price-file sim_price.csv -o stats_node_rb -N 50 --actor-fc-layer-params 20 16 --critic-fc-layer-params 20 16 16 --replay-buffer-capacity 1600

# 2x nodes, step
python ddpg.py --price-file sim_price.csv -o stats_node_step -N 50 --actor-fc-layer-params 20 16 --critic-fc-layer-params 20 16 16 -d step -i 20000

# 2x nodes, batch
python ddpg.py --price-file sim_price.csv -o stats_node_batch -N 50 --actor-fc-layer-params 20 16 --critic-fc-layer-params 20 16 16 --sample-batch-size-pct 0.5

# Round 3

# 4x nodes, 800 iterations
python ddpg.py --price-file sim_price.csv -o stats_node_4_800i -N 50 --actor-fc-layer-params 40 32 --critic-fc-layer-params 40 32 32 -i 800

# 4x nodes, 200 iterations
python ddpg.py --price-file sim_price.csv -o stats_node_4_200i -N 50 --actor-fc-layer-params 40 32 --critic-fc-layer-params 40 32 32 -i 200

# 8x nodes
python ddpg.py --price-file sim_price.csv -o stats_node_8 -N 50 --actor-fc-layer-params 80 64 --critic-fc-layer-params 80 64 64

# 16x nodes
python ddpg.py --price-file sim_price.csv -o stats_node_16 -N 50 --actor-fc-layer-params 160 128 --critic-fc-layer-params 160 128 128

# Show end time
echo "The script ended at $(date)."