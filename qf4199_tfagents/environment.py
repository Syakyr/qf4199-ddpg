from os import environ
environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import numpy as np
import pandas as pd
from collections import deque

from tf_agents.environments.py_environment import PyEnvironment
from tf_agents.specs.array_spec import BoundedArraySpec, ArraySpec
from tf_agents.trajectories import time_step as ts

tf.compat.v1.enable_v2_behavior()

envs = {}

# Simulated Market Environment
class SimMktEnv(PyEnvironment):

    def __init__(
        self, initial_balance=1., 
        mu=0.5, sigma=0.4, 
        r=0.02, T=1, delta=1/252, S0=1., target=1.4,
        use_price=False, 
        #use_w=False, alpha=0.001,
        file=None, cycle_period=100,
        
    ):

        self.initial_balance = initial_balance
        self.mu = (1+mu)**delta-1
        self.sigma = (1+sigma)**np.sqrt(delta)-1
        self.r = (1+r)**delta-1
        self.T = T
        self.delta = delta
        self.n = round(T/delta)
        self.S0 = S0
        self.target = target
        self._w = 0
        self.alpha = 0#alpha # Not used
        self._terminal_wealth_mean = 0
        self.run_counter = 0
        if file is not None:
            self.file = pd.read_csv(file)
        else:
            self.file = None
        self.file_counter = -1
        self.cycle_period = cycle_period

        self._use_price = use_price
        self._use_w = False#use_w # Not used
        
        self._state  = self.initial_balance
        self._state2 = lambda: [self._state-self._w, self._price] \
                               if use_price else [self._state-self._w]
        self.reset()

    def action_spec(self):
        return BoundedArraySpec(
            shape=(), dtype=np.float32,
            minimum=-1000*self.initial_balance, 
            maximum=1000*self.initial_balance,
            name='action'
        )

    def observation_spec(self):
        return ArraySpec(
            shape=(1 + self._use_price,), dtype=np.float32,
            name='observation'
        )

    def global_reset(self):
        self.file_counter = -1
        self._terminal_wealth_mean = 0
        self.run_counter = 0
        self._w = 0
        self.reset()
    
    def _reset(self):
        terminal_state, self._state = self._state, self.initial_balance
        if self._use_w:
            self._terminal_wealth_mean = (self._terminal_wealth_mean*self.run_counter + terminal_state)\
            /(self.run_counter+1)
            self.run_counter += 1
            self._w -= self.alpha*(self._terminal_state_mean-self.target)
        if self.file is None:
            self._price = self.S0
        else:
            self.file_counter += 1
            self.file2 = deque(
                self.file[str(self.file_counter % min(
                    self.cycle_period,self.file.shape[1]
                ))]
            )
            self._price = self.file2.popleft()
        self._time  = 0
        self._mean  = 0
        self._var   = 0
        self._episode_ended = False
        return ts.restart(np.array(self._state2(), dtype=np.float32))

    def _step(self, action):
        if self._episode_ended:
            # The last action ended the episode. Ignore the current action and start
            # a new episode.
            return self.reset()
        
        self._time += 1
        # Make sure episodes don't go on forever.
        if self._time > self.n:
            self._episode_ended = True
        else:
            units_allocated = action / self._price
            risk_free_profit = (self._state - action) * (1+self.r)
            if self.file is None:
                self._price *= 1+np.random.normal(self.mu, self.sigma)
            else:
                self._price = self.file2.popleft()
            prev_state = self._state
            self._state = risk_free_profit + units_allocated*self._price
            state_diff = self._state - prev_state
            prev_mean = self._mean
            self._mean = (self._mean * (self._time-1) + state_diff)/self._time
            prev_var = self._var
            self._var = self._var + prev_mean**2 - self._mean**2 + \
                ((state_diff**2-self._var-prev_mean**2)/self._time)
            

        if self._episode_ended:
            reward = 0
            # If agent doesn't reach the target by the end of the 
            # episode, heavily penalize them
            target_diff = self._state/self.initial_balance - self.target**self.T
            if target_diff <= 0:
                reward += target_diff * 100
            # Otherwise, we reward for sharpe ratio greater than 1
            else:
                reward += (self._state/self.initial_balance - 1)/\
                          ((1+self._var)**np.sqrt(1/self.delta)-1)
            return ts.termination(np.array(self._state2(), dtype=np.float32), reward)
        else:
            reward = 0
            # If agent doesn't reach the mini-target by the end of the 
            # episode, penalize them
            target_diff = self._state/self.initial_balance - self.target**(self._time*self.delta)
            if target_diff <= 0:
                reward += target_diff
            # Otherwise, we reward for sharpe ratio greater than 1 (after self._time > 1)
            elif self._time > 1:
                reward += (self._state/self.initial_balance - 1)/\
                          ((1+self._var)**np.sqrt(self._time)-1) * self._time * self.delta
            return ts.transition(
                np.array(self._state2(), dtype=np.float32), reward
            )
envs['SimMktEnv'] = SimMktEnv

# Data-based Market Environment
class DataMktEnv(PyEnvironment):
    def __init__(
        self, file,
        initial_balance=1., r=0.02, 
        target=1.4, use_price=False
    ):
        pass
    
#envs['DataMktEnv'] = DataMktEnv