def base_fn_valid(dct,s):
    from argparse import ArgumentTypeError
    if s.lower() in dct:
        return dct[s]
    else:
        lst = ', '.join(dct.keys())
        msg = f'{s} is not supported. Available: {lst}'
        raise ArgumentTypeError(msg)
        
def valid_environment(s):
    from qf4199_tfagents.environment import envs
    return base_fn_valid(envs, s)
    
def valid_optimizer(s):
    from tensorflow.compat.v1.train import AdamOptimizer, AdagradOptimizer
    optimizers = {
        'adam': AdamOptimizer,
        'adagrad': AdagradOptimizer
    }
    return base_fn_valid(optimizers, s)
    
def valid_driver(s):
    from tf_agents.drivers.dynamic_step_driver import DynamicStepDriver
    from tf_agents.drivers.dynamic_episode_driver import DynamicEpisodeDriver
    drivers = {
        'step': DynamicStepDriver,
        'episode': DynamicEpisodeDriver
    }
    return base_fn_valid(drivers, s)

class PrioritisedReplayBuffer:
    pass