# DDPG using `tf_agents`

For QF4199

## Installation

### Using Conda

To get all the required packages, run:

> `conda env create -f environment.yml`

in this folder.  
This will create the `tf2tutorial ` environment in your Anaconda installation. you can use that environment by activating it:

> `conda activate tf2tutorial`

Change the name of the environment in the yml file as you see fit.

### JupyterLab - Extra Steps

The environment uses `ipywidgets` to make `tqdm` pretty in notebooks. To show that in 
JupyterLab, you need to install this extension as well:

> `jupyter labextension install @jupyter-widgets/jupyterlab-manager`

### Installing Training Environment

If you want to use the environment in another project, you can do this
in the root of this repository:

> `conda develop .`

Then you can import the environment where you see fit:

> `from qf4199_tfagents.environment import SimMktEnv`

### Using CPU

If you can't run the notebook due to a lack of GPU, or GPU not supported,
you can do this to use CPU to train instead:

> `conda remove tensorflow-gpu`

## Running the Code

Read up on the different options you can choose to run the algorithm by running

> `python ddpg.py -h`

## Statistics Used

If you wish to use the statistics and price used in the paper, download them [here](/uploads/cdd0b13b91099ec45edb52af2759d435/stats.zip).